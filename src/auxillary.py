import os
import requests
from dotenv import load_dotenv
load_dotenv()

class AUX():
	BOT_TOKEN = os.getenv('BOT_TOKEN')

	#zee5 methods
	
	headers = {
		"User-Agent":"Mozilla/5.0 (Linux; Android 10; SM-J400F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Mobile Safari/537.36",
		"Referer":"https://www.zee5.com",
		"Accept":"*/*",
		"Accept-Encoding":"gzip, deflate, br",
		"Accept-Language":"en-US,en;q=0.9",
		"Origin":"https://www.zee5.com",
		"sec-fetch-dest":"empty",
		"sec-fetch-mode":"cors",
		"sec-fetch-site":"same-site",
	}

	token_url1 = "https://useraction.zee5.com/tokennd"
	search_api_endpoint = "https://gwapi.zee5.com/content/details/"
	platform_token = "https://useraction.zee5.com/token/platform_tokens.php?platform_name=web_app"
	token_url2 = "https://useraction.zee5.com/token"
	stream_baseurl = "https://zee5vodnd.akamaized.net"

	def zee5(self, link):
		code = link.split('/')[-1]
		try:
			req_token = requests.get(self.token_url1, headers=self.headers).json()
			req_platform_token = requests.get(self.platform_token).json()["token"]
			self.headers["X-Access-Token"] = req_platform_token
			request1 = requests.get(self.search_api_endpoint + code,headers=self.headers, params={"translation":"en", "country":"IN"}).json()
			pen = (request1["hls"][0].replace("drm", "hls") + req_token["video_token"])
			return self.stream_baseurl+pen
		except:
			try:
				req_token = requests.get(self.token_url2, headers=self.headers).json()
				req_platform_token = requests.get(self.platform_token).json()["token"]
				self.headers["X-Access-Token"] = req_platform_token
				request1 = requests.get(self.search_api_endpoint + code,headers=self.headers, params={"translation":"en", "country":"IN"}).json()
				pen = (request1["hls"][0].replace("drm", "hls") + req_token["video_token"])
				return self.stream_baseurl+pen
			except:
				return None