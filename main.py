from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters)
from telegram import (ParseMode, ChatAction, InlineKeyboardButton)
from src import MSG, auxillary

au = auxillary.AUX()

def start(update, context):
	# sends typing action before sending message
	context.bot.send_chat_action(chat_id=update.effective_chat.id, action=ChatAction.TYPING)
	# send welcome messsage
	context.bot.send_message(chat_id=update.effective_chat.id, text=MSG.START.format(update.message.from_user.first_name), parse_mode=ParseMode.HTML)

def zee(update, context):
	# sends typing action before sending message
	context.bot.send_chat_action(chat_id=update.effective_chat.id, action=ChatAction.TYPING)
	sent_msg = context.bot.send_message(chat_id=update.effective_chat.id, text=MSG.ANALYZING, parse_mode= ParseMode.HTML)
	stream_link = au.zee5(update.message.text)
	if stream_link != None:
		msg = MSG.SUCCESS.format(stream_link)
	else:
		msg = "Invalid URL!🤷"
	sent_msg.edit_text(msg, parse_mode=ParseMode.HTML)

def main():
	# init bot
	updater = Updater(token=au.BOT_TOKEN, workers=100, use_context=True)
	dp = updater.dispatcher

	# define handlers
	start_handler = CommandHandler('start', start)
	dp.add_handler(start_handler)
	message_handler = MessageHandler(Filters.regex('^https://www.zee5.com/'), zee)
	dp.add_handler(message_handler)

	# fire up the bot
	updater.start_polling()
	updater.idle()

if __name__ == "__main__":
	main()